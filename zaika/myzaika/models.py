from django.db import models
from django.contrib.auth.models import User

class customer(models.Model):
    user = models.OneToOneField(User,on_delete=models.CASCADE)
    contact = models.IntegerField()
    address = models.TextField()
    registred_on = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.user.username
