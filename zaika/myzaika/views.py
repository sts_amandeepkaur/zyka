from django.shortcuts import render
from django.http import HttpResponse
from django.contrib.auth.models import User
from myzaika.models import customer

# Create your views here.
def index(request):
    return render(request, 'home.html')
def login(request): 
    return render(request, 'login.html')
def signup(request):
    return render(request, 'signup.html')
def signupdata(request):
    if request.method == 'GET':
        first = request.GET['first_name']
        last = request.GET['last_name']
        email = request.GET['email']
        username = request.GET['username']
        password = request.GET['password']
        contact = request.GET['contact']
        address = request.GET['address']

        user_obj = User.objects.create_user(username,email,password)
        user_obj.first_name = first
        user_obj.last_name = last
        user_obj.save()

        cust = customer(user=user_obj,contact=contact,address=address)
        cust.save()
    return HttpResponse('Registred Successfully!!!')